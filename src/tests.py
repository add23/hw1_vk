import pytest
import httpretty
from logging import config

from utils import assert_and_log
from vk_client import VKClient

config.fileConfig('test_logging.conf')


def test_assert_and_log():
  # should not throw assert
  assert_and_log(True, '')

  # should throw assert
  try:
    assert_and_log(False, 'assert false')
    assert False
  except AssertionError:
    assert True


@httpretty.activate(verbose=True, allow_net_connect=False)
def test_vk_client():
  url = 'http://api.vk.ru/method/test'

  # good response
  httpretty.register_uri(
      httpretty.GET, url,
      body='{"response": [1,2,3]}'
  )

  client = VKClient(oauth_token='oauth_token')
  response = client.request(method='test', params={})
  assert response == [1, 2, 3]

  # bad response, should throw
  httpretty.register_uri(
      httpretty.GET,
      'http://api.vk.ru/method/test',
      body='{"error": ""}'
  )

  try:
    response = client.request(method='test', params={})
    assert False
  except AssertionError:
    assert True
