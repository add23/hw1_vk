import logging


def assert_and_log(cond: bool, msg: str) -> None:
  """Asserts and logs error if assertion fails

  Args:
      cond (bool): assert condition
      msg (str): error message

  Raises:
      err: assert error
  """
  try:
      assert cond, msg
  except AssertionError as err:
      logging.critical(msg)
      raise err
