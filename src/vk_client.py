
import logging
from logging import config
import requests

from utils import assert_and_log

VK_API_URL = 'http://api.vk.ru'
VK_API_VERSION = '5.1999'


class VKClient():
  """
  Vk client for accessing vk api
  """

  def __init__(self, oauth_token: str) -> None:
    self.oauth_token = oauth_token

  @staticmethod
  def prepare_params(oauth_token: str,  params: dict) -> dict:
    """Prepares params for vk api request

    Args:
        params (dict): original params

    Returns:
        dict: original params with necessary fields added
    """

    prepared = params.copy()
    prepared['access_token'] = oauth_token
    prepared['v'] = VK_API_VERSION
    logging.debug(f'params prepared: {prepared}')
    return prepared

  def request(self, method: str, params: dict) -> dict:
    """Sends request with `params` to vk api `method`

    Args:
        method (str): requested method (see vk api doc)
        params (dict): requested method params (see vk api doc)

    Returns:
        dict: response if request is successful
    """

    url = f'{VK_API_URL}/method/{method}'
    logging.info(f'Requested {url}')
    logging.debug(f'Request params {params}')

    params = VKClient.prepare_params(self.oauth_token, params)
    response = requests.get(url, params)

    assert_and_log(response.status_code == 200,
                   f'vk request failed, response code: {response.status_code}')
    logging.debug(f'{url} requested successfully')

    response_json = response.json()
    assert_and_log('response' in response_json,
                   f'vk request failed, response: {response_json}')
    logging.debug(
        f'response:{str(response_json):.100}'
        f'{"truncated..." if len(str(response_json)) > 100 else ""}'
    )

    return response_json['response']
